# Описание
## Как развернуть

``` bash
git clone https://gitlab.com/bansh1990/nginx-static-page-infrastructure.git
cd nginx-static-page-infrastructure/terraform
cp main.tf.example main.tf
# заполнить необходиммые поля в main.tf
terraform init
```
после этого скачаются необходимые модули и можно создавать workspace:

```bash
terraform workspace new stage
terraform workspace select stage
```

далее необходимо создать ключ для созданного сервисного аккаунта, который имеет доступ для создания виртуалок:
```bash
yc iam key create --service-account-name <acc_name> --output ../key.json --folder-id <folder ID>
```

так же для того чтобы наш репозиторий с приложением имел доступ к деплою на наш кластер нам необходимо создать агента gitlab:
![](images/20221120154955.png)  
![](images/20221120155132.png)  
![](images/20221120155227.png)  
копируем токен. нам он понадобится при развертывании кластера с помощью `terraform`.
```bash
terraform plan
terraform apply -var "yc-key=<путь/к/ключу/для/облака>" -var "gitlab_agent_token=<токен-гитлаб-агента>"
```
`terraform` нам создаст три ноды, а kubespray накатит на них кластер k8s с настройками из inventory `./templates/mycluster`.
а так же развернет стак ПО для Графаны, которая будет доступна по внешнему адресу второй или третьей ноды и доменному имени
grafana.diplom.ru (можно прописать в /etc/hosts для проверки)

теперь проверим доступностькластера:
![](images/20221120171027.png)  
![](images/20221120171122.png)  
все работает.
и агент гитлаба тоже подключен
![](images/20221120171214.png)  

а значит нам осталось только развернуть релиз, создав тэг в гитлабе

pipeline выполнился и приложение развернуто:
![](images/20221120171453.png)  
![](images/20221120171531.png)  

изменим цвет фона на синий и создадим тэг чтобы релиз попал на кластер

![](images/20221120171858.png)  
![](images/20221120171957.png)  
готово.

