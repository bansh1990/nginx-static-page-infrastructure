output "external_ip_kubernetes1" {
  value = yandex_compute_instance.kubernetes1.network_interface.0.nat_ip_address
}

output "external_ip_kubernetes2" {
  value = yandex_compute_instance.kubernetes2.network_interface.0.nat_ip_address
}

output "external_ip_kubernetes3" {
  value = yandex_compute_instance.kubernetes3.network_interface.0.nat_ip_address
}

output "internal_ip_kubernetes1" {
  value = yandex_compute_instance.kubernetes1.network_interface.0.ip_address
}

# resource "template_file" "ansible_inventory" {
#   template = file("../templates/inventory.tmpl")
#   vars = {
#     KUBER1             = yandex_compute_instance.kubernetes1.network_interface.0.nat_ip_address
#     KUBER2            = yandex_compute_instance.kubernetes2.network_interface.0.nat_ip_address
#     KUBER3            = yandex_compute_instance.kubernetes3.network_interface.0.nat_ip_address
#     KUBER_INTERNAL_1  = yandex_compute_instance.kubernetes1.network_interface.0.ip_address
#     KUBER_INTERNAL_2  = yandex_compute_instance.kubernetes2.network_interface.0.ip_address
#     KUBER_INTERNAL_3  = yandex_compute_instance.kubernetes3.network_interface.0.ip_address
#   }

# }


data "template_file" "ansible_inventory" {
  template = "${file("${path.module}/../templates/inventory.tmpl")}"
  vars = {
    KUBER1             = yandex_compute_instance.kubernetes1.network_interface.0.nat_ip_address
    KUBER2            = yandex_compute_instance.kubernetes2.network_interface.0.nat_ip_address
    KUBER3            = yandex_compute_instance.kubernetes3.network_interface.0.nat_ip_address
    KUBER_INTERNAL_1  = yandex_compute_instance.kubernetes1.network_interface.0.ip_address
    KUBER_INTERNAL_2  = yandex_compute_instance.kubernetes2.network_interface.0.ip_address
    KUBER_INTERNAL_3  = yandex_compute_instance.kubernetes3.network_interface.0.ip_address
  }
}
resource "local_file" "ansible_inventory" {
    content  = data.template_file.ansible_inventory.rendered
    filename = "../templates/mycluster/hosts.ini"


    provisioner "local-exec" {
      command = "git clone --depth 1 --branch v2.20.0 https://github.com/kubernetes-sigs/kubespray ../kubespray || echo 'allready cloned'"
    }
    provisioner "local-exec" {
      command = "python3 -m pip install -r ../kubespray/requirements.txt"
    }
    provisioner "local-exec" {
      command = "sleep 30 && ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i ../templates/mycluster/hosts.ini --become ../kubespray/cluster.yml -e etcd_retries=15"
    }
    provisioner "local-exec" {
      command = "rm -r ../kubespray"
    }
}

resource "null_resource" "update_admin_conf" {
    depends_on = [
      local_file.ansible_inventory
    ]
    triggers = {
    always_run = "${yandex_compute_instance.kubernetes1.network_interface.0.nat_ip_address}"
  }
    provisioner "local-exec" {
      command = "sed -i -- \"s/${yandex_compute_instance.kubernetes1.network_interface.0.ip_address}/${yandex_compute_instance.kubernetes1.network_interface.0.nat_ip_address}/g\" ../templates/mycluster/artifacts/admin.conf"
    }
 }