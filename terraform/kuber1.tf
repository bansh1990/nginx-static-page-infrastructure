
resource "yandex_compute_instance" "kubernetes1" {
  allow_stopping_for_update = true
  name = "kubernetes1"
  metadata = {
    ssh-keys = "debian:${file("~/.ssh/id_rsa.pub")}"
  }
  resources {
    cores  = 8
    memory = 8
  }
  boot_disk {
    initialize_params {
      image_id = "fd8kdnltr2353cirte81"
      size     = "15"
    }
  }
  network_interface {
    subnet_id  = yandex_vpc_subnet.netology-subnet-ext.id
    nat        = true
  }
}
