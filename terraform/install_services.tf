
resource "null_resource" "install_grafana" {
    depends_on = [
      null_resource.update_admin_conf
    ]
    provisioner "local-exec" {
      command = "git clone https://github.com/prometheus-operator/kube-prometheus ../kube-prometheus || echo 'allready cloned'"
    }

    provisioner "local-exec" {
      command = "../templates/mycluster/artifacts/kubectl.sh apply --server-side -f ../kube-prometheus/manifests/setup "
    }
    provisioner "local-exec" {
      command = "../templates/mycluster/artifacts/kubectl.sh wait --for condition=Established --all CustomResourceDefinition 	--namespace=monitoring"
    }

    provisioner "local-exec" {
      command = "../templates/mycluster/artifacts/kubectl.sh apply -f ../kube-prometheus/manifests "
    }

    provisioner "local-exec" {
      command = "../templates/mycluster/artifacts/kubectl.sh apply -f ../templates/grafana_expose "
    }
    provisioner "local-exec" {
      command = "rm -rf ../kube-prometheus "
    }
 }

 resource "null_resource" "install_gitlab_agent" {
  depends_on = [
    null_resource.install_grafana
  ]
  provisioner "local-exec" {
    command = "git clone https://gitlab.com/gitlab-org/charts/gitlab-agent ../gitlab-agent"
    
  }
  provisioner "local-exec" {
  command = <<EOT
    helm upgrade --kubeconfig ../templates/mycluster/artifacts/admin.conf --install agent ../gitlab-agent \
    --namespace gitlab-agent \
    --create-namespace \
    --set image.tag=v15.6.0-rc2 \
    --set config.token=${var.gitlab_agent_token} \
    --set config.kasAddress=wss://kas.gitlab.com
    EOT
    
  }
  provisioner "local-exec" {
  command = "rm -rf ../gitlab-agent"
  }
 }